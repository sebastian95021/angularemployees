import { Component, OnInit } from '@angular/core';
import { EmployeeModel } from 'src/app/models/employee.model';
import { EmployeeService } from 'src/app/services/employee.service';
import { JobsComponent, Job } from '../jobs/jobs.component';

@Component({
  selector: 'app-table',
  templateUrl: './listemployees.component.html'
})
export class ListEmployeesComponent implements OnInit {

  employees: EmployeeModel[] = [];
  employeesResult: EmployeeModel[] = [];
  allJobsComponent = new JobsComponent();
  allJobs: Job[];

  constructor(private employeeService: EmployeeService) {
  }

  ngOnInit() {
    // get all jobs
    this.allJobs = this.allJobsComponent.allJobs;

    // get all employees
    this.employeeService.getEmployees()
      .subscribe(res => {
        this.employees = res;
        this.employeesResult = res;
      });
  }

  // delete employee
  deleteEmployee(id: string, i: number) {
    this.employees.splice(i, 1);
    this.employeeService.deleteEmployee(id).subscribe();
  }

  // onkeyup event search
  search(event: any) {
    const value = event.target.value;
    this.employeesResult = this.employees.filter(emp => {
      return emp.name.toLowerCase().includes(value) ||
      String(emp.age).toLowerCase().includes(value) ||
      emp.username.toLowerCase().includes(value) ||
      emp.hireDate.toLowerCase().includes(value) ||
      this.allJobs[emp.jobTitle].name.toLowerCase().includes(value);
    });
  }

}


