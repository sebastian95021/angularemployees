import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup, AbstractControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { EmployeeModel } from 'src/app/models/employee.model';
import { EmployeeService } from '../../services/employee.service';
import { CountryService } from '../../services/country.service';
import { JobsComponent, Job } from '../jobs/jobs.component';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html'
})
export class EmployeeComponent implements OnInit {

  employeeForm: FormGroup;
  employee = new EmployeeModel();
  allJobsComponent = new JobsComponent();
  allJobs: Job[];
  viewMode = false;
  countries: any[];
  areas: string[] = ['Services', 'Kitchen'];
  jobs: Job[];

  constructor(private fb: FormBuilder,
              private employeeService: EmployeeService,
              private countryService: CountryService,
              private route: ActivatedRoute,
              private router: Router) {
    this.employeeForm = this.fb.group({
      id: [null],
      name: ['', Validators.required],
      area: ['', Validators.required],
      dob: ['', [Validators.required, this.validateAge]],
      age: [null],
      jobTitle: ['', Validators.required],
      country: ['', Validators.required],
      username: ['', [Validators.required, Validators.pattern('([a-zA-Z0-9 ]*)')]],
      hireDate: ['', Validators.required],
      tipRate: [null],
      status: [true, Validators.required],
    });
  }

  validateAge(control: AbstractControl ) {
    const convertAge = new Date(control.value);
    const timeDiff = Math.abs(Date.now() - convertAge.getTime());
    const age = Math.floor((timeDiff / (1000 * 3600 * 24)) / 365);

    return (age >= 18) ? null : { ageMax: true };
  }

  ngOnInit() {
    // get variables path route
    const id = this.route.snapshot.paramMap.get('id');
    const view = this.route.snapshot.paramMap.get('view');
    if (view !== null) {
      this.viewMode = true;
      Object.keys(this.employeeForm.controls).forEach(controlName => {
        this.employeeForm.get(controlName).disable();
      });
    }

    // get countries
    this.countryService.getCountries()
    .subscribe( (res: any) => {
      this.countries = res;
    });

    // get all jobs
    this.allJobs = this.allJobsComponent.allJobs;

    // get employee
    if (id !== 'new') {
      this.employeeService.getEmployee(id)
      .subscribe( (res: EmployeeModel) => {
        this.employee = res;
        this.employee.id = id;
        this.employee.tipRate = (this.employee.tipRate === undefined) ? null : this.employee.tipRate;
        this.changeJobs(this.employee.area);
        this.employeeForm.setValue(this.employee);
      });
    }
  }

  onChangeArea() {
    const area: string = this.employeeForm.get('area').value;
    this.changeJobs(area);
  }

  changeJobs(area: string) {
    this.jobs = this.allJobs.filter(job => job.area === area);
  }

  // Save/Update employee from Form
  save(employee: EmployeeModel) {
    let request: Observable<any>;

    // Calculate age
    const convertAge = new Date(employee.dob);
    const timeDiff = Math.abs(Date.now() - convertAge.getTime());
    const age = Math.floor((timeDiff / (1000 * 3600 * 24)) / 365);
    employee.age = age;
    employee.tipRate = (![3, 4].includes(employee.jobTitle)) ? 0 : employee.tipRate;

    if (employee.id) {
      request = this.employeeService.updateEmployee(employee);
    } else {
      request = this.employeeService.addEmployee(employee);
    }

    request.subscribe( res => {
      console.log(res);
      this.router.navigate(['employees']);
    });
  }
}
