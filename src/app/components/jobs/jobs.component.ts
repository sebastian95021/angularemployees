import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-jobs',
  templateUrl: './jobs.component.html'
})
export class JobsComponent implements OnInit {

  allJobs: Job[];

  constructor() {
    this.allJobs = [{id: 0, name: 'Manager', area: 'Services'},
      {id: 1, name: 'Host', area: 'Services'},
      {id: 2, name: 'Tuttafare', area: 'Services'},
      {id: 3, name: 'Waitress', area: 'Services'},
      {id: 4, name: 'Dinning room manager', area: 'Services'},
      {id: 5, name: 'Chef. Sous Chef', area: 'Kitchen'},
      {id: 6, name: 'Dishwasher', area: 'Kitchen'},
      {id: 7, name: 'Cook', area: 'Kitchen'},
    ];
  }

  ngOnInit() {
  }

}

export interface Job {
  id: number;
  name: string;
  area: string;
}
