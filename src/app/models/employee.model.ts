export class EmployeeModel {
  id: string;
  name: string;
  age: number;
  area: string;
  dob: string;
  jobTitle: number;
  country: number;
  username: string;
  hireDate: string;
  tipRate: number;
  status: boolean;

  constructor() {
  }
}


  /*private employees: Employee[] = [
    {
      name: 'Giocomo Guilizoni Founder & CEO',
      age: 40,
      username: 'Peldi',
      hireDate: new Date('2017/10/01'),
      area: 0,
      dob: new Date('2017/10/01'),
      jobTitle: 0,
      country: 0,
      tipRate: null,
      status: true
    },
    {
      name: 'Marco Botton Tuttorlore',
      age: 38,
      username: 'Marcopolo',
      hireDate: new Date('2001/01/10'),
      area: 0,
      dob: new Date('2017/10/01'),
      jobTitle: 0,
      country: 0,
      tipRate: null,
      status: true
    },
    {
      name: 'Mariah Moclachlan Better Half',
      age: 41,
      username: 'Pototo',
      hireDate: new Date('2016/02/01'),
      area: 0,
      dob: new Date('2017/10/01'),
      jobTitle: 0,
      country: 0,
      tipRate: null,
      status: true
    },
    {
      name: 'Valerie Liberty Head Chef',
      age: 30,
      username: 'Vol',
      hireDate: new Date('2018/03/02'),
      area: 0,
      dob: new Date('2017/10/01'),
      jobTitle: 0,
      country: 0,
      tipRate: null,
      status: true
    },
  ]; */
