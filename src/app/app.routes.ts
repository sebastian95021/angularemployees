import { RouterModule, Routes } from '@angular/router';
import { ListEmployeesComponent } from './components/listemployees/listemployees.component';
import { EmployeeComponent } from './components/employee/employee.component';

const APP_ROUTES: Routes = [
  { path: 'employees', component: ListEmployeesComponent },
  { path: 'employee/:id', component: EmployeeComponent },
  { path: 'employee/:view/:id', component: EmployeeComponent },
  { path: 'employee/new', component: EmployeeComponent },
  { path: '**', pathMatch: 'full', redirectTo: 'employees'}
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);
