import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { EmployeeModel } from '../models/employee.model';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  private url = 'https://employees-b70ec.firebaseio.com';

  constructor(private http: HttpClient) { }

  // Create a new employee
  addEmployee(employee: EmployeeModel ) {
    return this.http.post(`${ this.url }/employees.json`, employee)
      .pipe(
        map( (res: any) => {
          employee.id = res.name;
          return employee;
        })
      );
  }

  // Update employee
  updateEmployee(employee: EmployeeModel ) {
    const employeeT = {...employee};
    delete employeeT.id;
    return this.http.put(`${ this.url }/employees/${employee.id}.json`, employeeT);
  }

  // Show detail employee
  getEmployee(id: string ) {
    return this.http.get(`${ this.url }/employees/${id}.json`);
  }

  // List all employees
  getEmployees() {
    return this.http.get(`${ this.url }/employees.json`)
      .pipe(
        map( res => this.createArray(res))
      );
  }

  deleteEmployee(id: string) {
    return this.http.delete(`${ this.url }/employees/${id}.json`);
  }

  // Create array valid for *ngFor
  private createArray(employeeObj: object) {
    const employees: EmployeeModel[] = [];

    if (employeeObj === null) { return []; }

    Object.keys( employeeObj ).forEach( key => {
      const employee: EmployeeModel = employeeObj[key];
      employee.id = key;

      employees.push( employee );
    });

    return employees;
  }
}
