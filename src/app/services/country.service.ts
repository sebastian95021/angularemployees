import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CountryService {

  private url = 'https://restcountries.eu/rest/v2/all?fields=numericCode;name;';

  constructor(private http: HttpClient) { }

  // List all employees
  getCountries() {
    return this.http.get(`${ this.url }`);
  }
}
